# Python 3: Async/Await

Requires python 3.5+

Google Slides:  https://docs.google.com/presentation/d/1fURtUCUYFb6BHhmhhnOJ_ooIjQZcRYa_VMljurGByVQ/edit?usp=sharing

#### 1. Clone the repo
```
git clone https://bitbucket.org/vickumar/python-async-await
```

#### 2. Setup Python 3 Virtual environment
```
virtualenv -p [path to python3 executable] venv
source venv/bin/activate
```

#### 3. Run the threads example
```
python threads.py
```

#### 4. Run the processes example
```
python processes.py
```

#### 5. Run `concurrent.futures` example
```
python futures.py
```

#### 6. Run the `requests` reddit example
```
pip install -r requirements.txt
python reddit2.py
```

#### 7. Run the `aiohttp` reddit example
```
pip install -r requirements.txt
python reddit.py
```

#### 8. Run the `django-channels` reddit example
Install `docker`:  https://docs.docker.com/install/

Install `docker-compose`:  https://docs.docker.com/compose/install/

```
cd redit_reader
docker-compose up
```