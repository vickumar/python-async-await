import asyncio
import json
import datetime
import re
from aiohttp import ClientSession
from channels.generic.http import AsyncHttpConsumer


class ReditConsumer(AsyncHttpConsumer):
    """
    Async HTTP consumer that fetches redits.
    """

    def clean_url(self, reddit):
        return re.sub('[^0-9a-zA-Z]+', '', str(reddit))

    def get_reddit_url(self, subreddit, limit=15):
        return "https://www.reddit.com/r/%s/top.json?sort=top&limit=%s" % (self.clean_url(subreddit), str(limit))

    async def get_json(self, client, url):  
        async with client.get(url) as response:
            return await response.read()

    def get_reddit(self, redit_json):        
        j = json.loads(redit_json.decode('utf-8'))
        responses = list()
        if "error" in j:
            print("\n%s\n" % j)
            return responses

        json_data = j['data']['children']
        for i in json_data:
            try:
                image_url = i['data']['preview']['images'][0]['source']['url']
            except:
                image_url = None
            title, link = i['data']['title'], i['data']['url']
            response = { "title": title, "link": link}
            if image_url:
                response["image_url"] = image_url
            responses.append(response)       
        return responses

    async def handle(self, body):        
        loop = asyncio.get_event_loop()
        t0 = datetime.datetime.now()
        tasks = list()
        reddits = [self.clean_url(r) for r in str(body).split("=")[-1].split("+")]
        async with ClientSession(loop=loop) as client:
            for r in reddits:
                print('Fetching subreddit "%s"' % r)
                url = self.get_reddit_url(r)
                task = loop.create_task(self.get_json(client, url))
                tasks.append(task)

            responses = await asyncio.gather(*tasks)            
            dt = (datetime.datetime.now() - t0).total_seconds()
            print('All subreddits completed; elapsed time: {} [s]'.format(dt))            

        response = {
            reddit: self.get_reddit(reddit_json) \
                for reddit, reddit_json in zip(reddits, responses)
        }
        text = json.dumps({ "reddits": response, "elapsed_time": round(dt, 3) })

        await self.send_response(200,
            text.encode(),
            headers=[
                ("Content-Type", "application/json"),
            ]
        )