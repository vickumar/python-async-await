import asyncio  
import aiohttp  
import json
import time    

redits = ['funny', 'AskReddit', 'todayilearned', 'soccer', 'worldnews']

def get_reddit_url(subreddit, limit=100):
    return "https://www.reddit.com/r/%s.json?sort=all&limit=%s" % (str(subreddit), str(limit))

async def get_json(client, url):  
    async with client.get(url) as response:
        assert response.status == 200
        return await response.read()

async def get_reddit(subreddit, event_loop, show_output=False):
    start = time.time()
    client = aiohttp.ClientSession(loop=event_loop)
    url = get_reddit_url(str(subreddit))
    data1 = await get_json(client, url)

    j = json.loads(data1.decode('utf-8'))
    json_data = j['data']['children']
    print("\n%s total results - %s" % (subreddit, len(json_data)))
    for i in json_data:
        title, link = i['data']['title'], i['data']['url']
        if show_output:
            print("#%s: %s (%s)" % (str(subreddit), title, link))
    print("DONE: %s\n" % subreddit)
    elapsed = time.time() - start
    await client.close()
    return elapsed

async def get_reddits(event_loop):    
    return await asyncio.gather(*(get_reddit(r, event_loop) for r in redits))


start = time.time()
event_loop = asyncio.get_event_loop()
t1, t2, t3, t4, t5 = event_loop.run_until_complete(get_reddits(event_loop))
print('>>>  Cumulative\t\t%s secs.' % (t1 + t2 + t3 + t4 + t5))
print('>>>  Total:\t\t%s secs.' % str(time.time() - start))
