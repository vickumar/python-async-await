
from django.shortcuts import render


def index(request):
    """
    Main page is just a template
    """
    return render(request, "index.html", {})
