from django.urls import path, re_path
from channels.http import AsgiHandler
from channels.routing import ProtocolTypeRouter, URLRouter
from redit_reader.consumers import ReditConsumer


application = ProtocolTypeRouter({
    "http": URLRouter([
        path("redit_reader/get_redits/", ReditConsumer),
        re_path("^", AsgiHandler),
    ]),
})